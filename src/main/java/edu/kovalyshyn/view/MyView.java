package edu.kovalyshyn.view;

import edu.kovalyshyn.controller.Controller;
import edu.kovalyshyn.controller.ControllerImpl;
import edu.kovalyshyn.model.LongestPlateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger log = LogManager.getLogger(MyView.class);


    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 -  PlateauTest");
        menu.put("2", " 2 -  Minesweeper");
        menu.put("Q", " Q - EXIT");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);

    }

    private void pressButton1() {
        String result;
        int[] test1 = {4, 1, 1, 6, 6, 6, 6, 1, 1};

        log.info("int[]= " + Arrays.toString(test1));
        result = Arrays.toString(controller.longestPlateau(test1));
        log.info("[ value , start at index , len ] = " + result + " \n");

    }

    private void pressButton2() {
        controller.minesweeper();
    }


    private void outputMenu() {
        System.out.println("\n MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                log.info("-EXIT- ");
            }
        } while (!keyMenu.equals("Q"));
    }
}
