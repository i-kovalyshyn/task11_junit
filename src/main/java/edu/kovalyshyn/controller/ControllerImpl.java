package edu.kovalyshyn.controller;

import edu.kovalyshyn.model.Model;
import edu.kovalyshyn.model.ModelImpl;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }

    @Override
    public int[] longestPlateau(int[] array) {
       return model.longestPlateau(array);
    }

    @Override
    public void minesweeper() {
        model.minesweeper();
    }


}
