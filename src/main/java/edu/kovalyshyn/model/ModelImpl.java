package edu.kovalyshyn.model;

public class ModelImpl implements Model {
    @Override
    public int [] longestPlateau(int[] array) {
       return new LongestPlateau().longestPlateau(array);
    }

    @Override
    public void minesweeper() {
        new Minesweeper().minesweeper();

    }


}
