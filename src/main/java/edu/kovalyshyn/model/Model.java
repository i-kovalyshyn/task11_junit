package edu.kovalyshyn.model;

public interface Model {
    int[] longestPlateau(int [] array);
    void minesweeper();

}
