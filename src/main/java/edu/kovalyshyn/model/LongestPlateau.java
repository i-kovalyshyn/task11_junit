package edu.kovalyshyn.model;

import java.util.ArrayList;

public class LongestPlateau {
    public  int[] longestPlateau(int[] arrayOfInts) {
        int start = 0;
        int value = 0;
        int repitation = 0;

        if (arrayOfInts.length == 1) {
            start = 0;
            value = arrayOfInts[0];
            repitation = 1;
        }
        ArrayList<Integer> length = new ArrayList<>();
        ArrayList<Integer> index = new ArrayList<>();
        ArrayList<Integer> plateau = new ArrayList<>();
        //Initialize the length array
        length.add(1);
        int[] k = new int[arrayOfInts.length];
        k[0] = arrayOfInts[0];
        int m = 1;
        index.add(0);

        for (int i = 1; i < arrayOfInts.length; i++) {
            if (arrayOfInts[i] != arrayOfInts[i - 1]) {
                k[m] = arrayOfInts[i];
                m++;
                index.add(i);
                //Initialize the length for 1, because we have a new element. so count the new beginning.
                length.add(1);
            } else if (arrayOfInts[i] == arrayOfInts[i - 1]) {
                length.add(m - 1, length.get(m - 1) + 1);
                length.remove(m);
            }
        }
        for (int z = 0; z < k.length - 1; z++) {
            if (z == 0) {
                if (k[z] > k[z + 1]) {
                    plateau.add(z);
                }
            } else if (z == k.length - 1) {
                if (k[z] > k[z - 1]) {
                    plateau.add(z);
                }
            } else if (k[z] > k[z + 1] && k[z] > k[z - 1]) {
                plateau.add(z);
            }
        }
        // very special case : when the platue array have only one possible platue in it then we can just do it like this
        if (plateau.size() == 1) {
            repitation = length.get(plateau.get(0));
            start=index.get(plateau.get(0));
            value=k[plateau.get(0)];
        }
        // Detecting longest platuaes and assigned values
        for (int d= 1; d<plateau.size(); d++){
            if (length.get(plateau.get(d))>length.get(plateau.get(d-1))){
                repitation=length.get(plateau.get(d));
                start=index.get(plateau.get(d));
                value=k[plateau.get(d)];
            }else if (length.get(plateau.get(d-1)) > length.get(plateau.get(d))){
                repitation = length.get(plateau.get(d-1));
                start = index.get(plateau.get(d-1));
                value = k[plateau.get(d-1)];
            }
        }
        int[] result = { value, start, repitation };
        return result;
    }

}