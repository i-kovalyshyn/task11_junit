package edu.kovalyshyn;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("JUnit tasks runner")
@SelectPackages("edu.kovalyshyn.model")
@IncludeClassNamePatterns({"^.*$"})
public class RunTask {
}
