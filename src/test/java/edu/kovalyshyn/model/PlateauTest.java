package edu.kovalyshyn.model;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class PlateauTest {
    private int[] givenArray;
    private int[] expectedResult;
    private LongestPlateau plateau;

    public PlateauTest(int[] givenArray, int[] expectedResult) {
        this.givenArray = givenArray;
        this.expectedResult = expectedResult;
    }

    @Before
    public void init() {
        plateau = new LongestPlateau();
    }

    @Parameterized.Parameters
    public static int[][][] data() {
        return new int[][][]{
                {{3, 3, 3, 4, 1, 2, 4, 4, 0, 1}, {4, 6, 2}},
                {{4, 4, 4, 5, 5, 5, 6, 6}, {6, 6, 2}},
                {{0, 0, 0, 0, 0, 0, 0, 1}, {1, 7, 1}},
                {{4}, {4, 0, 1}}
        };
    }

    @Test
    @DisplayName("Find the longest plateau")
    public void findTheLongestPlateauTest() {
        assertThat(plateau.longestPlateau(givenArray), is(expectedResult));
    }
}
